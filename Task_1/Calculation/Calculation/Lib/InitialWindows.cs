﻿namespace Calculation.Lib
{
    internal static class InitialWindows
    {
        const int 
            COUNT_NUMBER = 10,
            COUNT_COLUMNS = 3,
            INDENT = 3,
            SIZE_BUTTON = 20;

        internal static void SetInitialWindows(ref MainForm buff)
        {
            Panel panelNumber = createPanelNumbers();



            buff.Controls.Add(panelNumber);
        }

        private static Panel createPanelNumbers()
        {
            Panel panelNumber = new();

            // TODO исправь
            panelNumber.Location = new Point(10, 50);

            var arrButton = new Button[COUNT_NUMBER];

            for (int index = 0; index < COUNT_NUMBER; index++)
            {
                arrButton[index] = new Button();
                arrButton[index].Text = index.ToString();
                arrButton[index].Size = new Size(SIZE_BUTTON, SIZE_BUTTON);

                var _x = index % COUNT_COLUMNS;
                var _y = index / COUNT_COLUMNS;

                arrButton[index].Location =
                new Point(
                    _x * (SIZE_BUTTON + INDENT),
                    _y * (SIZE_BUTTON + INDENT));
            }

            panelNumber.Controls.AddRange(arrButton);

            return panelNumber;
        }
    }
}
